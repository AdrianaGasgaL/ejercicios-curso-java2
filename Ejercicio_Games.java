import java.util.Optional;

interface Gameable {
		void startGame();
		default void setGameName(String name) {
			
		}
	}
	
	interface Playeable {
		void walk(double x, double y);
		default void setGameName(String name){
			
		}
	}
	
	interface Soundable {
	 /*default void playMusic(String song){
		final Optional<String> os = Optional.of(song);
			if(!os.isPresent()) {
	        	System.out.println("cambio");
	        }else
	        {
	        	song = "othersong";
	        }
		}*/
		void playMusic(String song);
		default void setGameName(String name) {
			
		}
	}
	
public class Ejercicio_Games{
	static String gameName="game";
	static double playerPosx=1;
	static double playerPosY=1;
	static String song="xf";
	public static void main(String[] args)
	{
        Playeable p = (playerPosx, playerPosY) -> { //playerPosx, playerPosY
        	playerPosx +=1;
        	playerPosY +=1;
        	System.out.println(playerPosx +"\n" +  playerPosY);
        };
        p.walk(2,1);
        //p.setGameName("song0");
        Gameable g = () -> {
        	System.out.println("gameName: " + gameName);
        };
        g.startGame();        
        Soundable s = (gameName) -> {
        	System.out.println(gameName);
        };
        
    		final Optional<String> os = Optional.of(song);//empty();
    			if(!os.isPresent()) {
    	        	System.out.println("nulo");
    	        	song="cancioncambio";
    	        }else
    	        {
    	        	song = song;
    	        }
    		
        s.playMusic(song);
	}
}