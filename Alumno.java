import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Alumno {

	String nombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String nombreCurso;
	public Alumno(String nombre, String apellidoPaterno, String apellidoMaterno, String nombreCurso)
	{
		this.nombre=nombre;
		this.apellidoPaterno=apellidoPaterno;
		this.apellidoMaterno=apellidoMaterno;
		this.nombreCurso=nombreCurso;
	}
	public void setNombre(String nombre)
	{
		this.nombre=nombre;
	}
	public void setAP(String apellidoPaterno)
	{
		this.apellidoPaterno=apellidoPaterno;
	}
	public void setAM(String apellidoMaterno)
	{
		this.apellidoMaterno=apellidoMaterno;
	}
	public void setNomCurso(String nombreCurso)
	{
		this.nombreCurso=nombreCurso;
	}
	public String getNombre()
	{
		return nombre;
	}
	public String getAP()
	{
		return apellidoPaterno;
		
	}
	public String getAM()
	{
		return apellidoMaterno;
		
	}
	public String getNomCurso()
	{
		return nombreCurso;
	}
	public String toString() {
		return nombre+ " "+apellidoPaterno + " "+apellidoMaterno + " "+nombreCurso;
	}
	public static void main(String[] args) {
		List<Alumno>  lalumnos= new ArrayList<Alumno>();
		
		for (int i=0; i<19; i++){
			Alumno a = new Alumno("nomb"+i,"ap"+i,"am"+i,"curso");
			lalumnos.add(a);
		}
		Alumno a2 = new Alumno("Adriana","ap","am","curso"); lalumnos.add(a2);
		
		lalumnos.forEach(l -> System.out.println(l.toString()));
		System.out.println("Longitud lista: "+ lalumnos.stream().count());
		
		System.out.println("Alumnos cuyo nombre termine con la letrá \"a\"");
		lalumnos.stream().filter(le -> le.getNombre().charAt(le.getNombre().length()-1)=='a').forEach(le -> System.out.println(le));
			
		System.out.println("Primeros 5");
		lalumnos.stream().limit(5).forEach(c -> System.out.println(c));
	}

}
